# Simulating and inferring selection module 

### [Claudia Bank's](https://evoldynamics.org/) exercise module at the [2018 Workshop on Population and Speciation Genomics](http://evomics.org/workshops/workshop-on-population-and-speciation-genomics/2018-workshop-on-population-and-speciation-genomics-cesky-krumlov/) in Český Krumlov

### To setup the environments follow the description in the first section on the [accompanying website](https://evoldyn.gitlab.io/evomics-2018/) 

## https://evoldyn.gitlab.io/evomics-2018/