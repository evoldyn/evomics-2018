---
title: "Simulating and inferring selection"
output: 
  html_notebook:
    theme: "cosmo"
    highlight: textmate
    toc: true
    toc_float: true
---

Authors: Claudia Bank, Alexandre Blanckaert, Mark Schmitz

# Validate your results via simulation

In the second part, you will use SLIM, a powerful forward-time simulation software, to validate your results. The point here is to create data according to your hypothesized scenarios, analyze these like above, and compare the results. SLIM is rather complex, which is both good and bad. On the positive side, you can implement almost any scenario you can think of and also get various forms of output, on the negative side, it takes a while to truly understand the code and go beyond the 'recipes' that are provided with the software. For the purpose of today, we have provided you with a number of ready-made simulation scenarios that you can use right away, but we encourage you to play with the code. More info about SLIM here: https://messerlab.org/slim/ 

You will be using the command-line version here. Compiled binaries should be available in the folder `./bin/`

## The challenge

[Here (PDF, 32 KB)](WFABC_data/challenge_data/selection_inference_challenge.pdf) you can find the list of true scenarios, under which the data for Part 1 of the exercise were simulated. The goal is to identify which scenario your data set belonged to, and to enter the respective number in the sheet. [Google Docs Version](https://docs.google.com/spreadsheets/d/1bGakVjtphQVcN6zZC0-m47kffMxfN11Yp6Qlp25cpEs/edit?usp=sharing)

## Run SLIM

We have prepared a number of simulation scenarios for you that you can play with and extend as desired.  We have commented most files extensively to allow you to easily change the parameters.  The simulation code is contained in the folder `SLIM_Simulations`. You run e.g. the simulation `MY_SIMULATION.slim` by executing

`./bin/slim MY_SIMULATION.slim`

```{bash}
./bin/slim ./SLIM_simulations/selection.slim
echo
echo "Exit code (should be 0): "$?
echo
```
Note that the way we specified the file path, SLIM deposits the output directly in your working directory. To keep things in order, we will copy the output to a new folder in the next step.

Some hints for trying out parameters:
+ simulations will become slower with increasing population size
+ only if $N\cdot s>1$ selection has a chance!

## Create a wfabc input file from SLIM output

The SLIM output is rather complex. In order to run WFABC and to visualize the allele-frequency trajectories, you have to convert the complex output from SLIM into the WFABC input format. SlimToWfabc.py does this job taking the following parameters:

+ `input file`: output you received from SLIM (e.g., "selection.dat")
+ `output_for_Wfabc_file`: desired name of wfabc file
+ `position_file`: desired name for file that contains genome positions
+ `threshold_to_remove_rare_allele`: minimum frequency (as sample number) that has to be reached at least once to make it into the file - this is to avoid huge files and thus reduce run time of WFABC.

To run this, execute
`python SlimToWfabc.py input_file output_for_Wfabc_file position_file threshold_to_remove_rare_allele`

```{bash}
# Check if working directory is correct
# Get only the last part of working directory (which should be "./public" here)
current_dir=$(pwd | awk -F/ '{print $NF}')
if [[ $current_dir != "public" ]]; then
  echo "This chunk of code should only be run from the ./public/ directory of this module";
  exit
fi

# Specify name of the folder that you want to use for this simulation and downstream WFABC analysis - we recommend a meaningful name
NAME=Selection_1
# create the directory if it does not exist yet
if [[ ! -d $NAME ]]; then
  echo "Create folder $NAME to use for this simulation and downstream WFABC analysis"
  mkdir -pv $NAME
fi

# move your SLIM output here
mv ./slim_output.dat ./$NAME/slim_output.dat
mv ./slim_output_fixedmut.dat ./$NAME/slim_output_fixedmut.dat
# create WFABC input and a file with allele-frequency trajectories
python ./SlimToWfabc.py ./$NAME/slim_output.dat ./$NAME/slim_output_fixedmut.dat ./$NAME/input_wfabc.txt ./$NAME/positions.txt 5

# Now you can start the WFABC analysis and anything else you would like to analyze to compare between your original and the simulated data set. For example:
# Run wfabc_1 from the ./bin directory using input from your data directory
./bin/wfabc_1 ./$NAME/input_wfabc.txt
echo
echo "Exit code (should be 0): "$?
echo
```

# Analysis

There are systematic ways of testing which model scenario most likely resulted in your data set. However, the task today is to do the model selection mainly "by eye" and with the help of your results from WFABC. You have probably already realized in Part 1 of the exercise that it is essential to *look* at the data, which means to take a critical look at the allele frequency trajectories. Here is the code for the visualization of the trajectories again, this time using the `positions.txt` file that is part of the output from the SLIM-to-WFABC conversion. It allows you to also see the genome position of the segregating loci. If you look at the trajectories of a couple of outputs from the simulations, you will get a feeling for each scenario, and on how different parameters change the trajectories.

```{r}
# import allele-frequency data
position_data <- read.table("./Selection_1/positions.txt", header=TRUE)
# extract number of time 
no_timepoints <- length(position_data[1,])-2
# extract number of mutants
no_mutants <- length(position_data[,1])
# extract frequency information
frequencies <- position_data[3:(no_timepoints+2)]
# define x axis range
xrange <- c(1,no_timepoints) 
# define y axis range
yrange <- c(0,1)
# set up plot
plot(xrange, yrange, type="n", xlab="Time point", ylab="Frequency" ) 
# define colors to be used
colors <- rainbow(no_mutants) 
# add lines for every mutant
for (i in 1:no_mutants) { 
  lines(1:no_timepoints, frequencies[i,], type="l", lwd=1.5, col=colors[i]) 
} 
# add title
title("Allele-frequency trajectories")
# add legend 
legend("topright", yrange[2], position_data[,1],lty=1, cex=0.8, col=colors, title="Position")
```
The `positions.txt` file also contains the information which of the observed trajectories were selected (usually the 'm2' mutations) and which were neutral ('m1').


# Final questions

> + What helped you most to identify your selection scenario?
> + How powerful is WFABC for detecting selection?
> + Any suggestions on how to implement methods that can distiguish the presented selection scenarios?
> + How useful/necessary was the validation exercise to understand your data?
> + What are the up- and downsides of time-serial data?
> + Which complications do you expect if you were dealing with real instead of simulated data?
> + Any questions/concerns/comments?

## Where to go now?
[Full site map](sitemap.html) also containing the ![](img/pdf.png) ["positive and negative selection" lecture (PDF 6 MB)](lecture/positive_and_negative_selection.pdf) as well as [solutions to the challange (PDF 23 KB)](for_instructors_only/selection_inference_challenge_solutions.pdf), and instructors only version.
