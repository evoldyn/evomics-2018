import sys

#check that the command line has four element besides python and the script name
if len(sys.argv)!=6:
	print "need an input file, the fixed mutations file, an output file for WFABC, an ouput file for saving posisiton, and the threshold of deletion"
	exit()

#open fixed mutation file
data_fm=open(sys.argv[2],'r');

# empty dictionary to store the fixed mutation 
fix_mut={};

#read the input file line by line
for line in data_fm:
	# split each line by spaces into an array called "a"
	a=line.rsplit(' ');
	if a[0].isdigit():
		# if the first element is a digit, save the fixed mutations and when they fixed
		fix_mut[a[1]]=int(a[8].strip('\n'));


# open input file (first commandline parameter)
data=open(sys.argv[1],'r');

# empty vector/array for generation time for each sampling event
vec_gen=[];

# empty dictionary to store the mutation 
mut={};

# counter for the number of sample
count=0;

# empty vector/array for sample sizes
sample=[];

# read the input file line by line
for line in data:
	# split each line by spaces into an array called "a"
	a=line.rsplit(' ');
	
	# detect for line output of sampling event and extract the generation time
	if (a[0]=='#OUT:'):
		vec_gen.append(int(a[1]));
		count=len(vec_gen)-1;
		sample.append(int(a[4]));
	
	# check that we are looking at mutation and not individuals
	elif a[0].isdigit():
	
		# for the first time, no need to check whether the mutation is already known
		if count==0:
		# write a list of: a vector containing the number for time the allele has been observed, the position of the mutation in the genome and the type of the mutation;
			mut[a[1]]=[[int(a[8].strip('\n'))],a[3],a[2]];
		
		# check if the mutation is known
		elif (a[1] in mut.keys()):
		
		# check if it was not observed at the previous sampling point and add as many 0 as missing observations
			while (len(mut[a[1]][0])!=count):
				mut[a[1]][0].append(0);
				# write the number for time the allele has been observed
			mut[a[1]][0].append(int(a[8].strip('\n')));
			
		else:
		# create new entry, add the missing observation (add 0) and the latest value
			temp=[0]*count;
			temp.append(int(a[8].strip('\n')));
			mut[a[1]]=[temp,a[3],a[2]];

count=count+1;

# go through the dictionary for missing last values and removing rare alleles

# assuming fixed pop size throughout, used for filtering out min >= .95 frq
sample_size=sample[0]

for i in mut.keys():
	# add aditional checks here, e.g. if the min is < .95 frq.
	# if ( min(mut[i][0])/float(sample[0])  > 0.95 ): print(mut[i][0])
	if ( max(mut[i][0])>int(sys.argv[5]) ) and ( min(mut[i][0])/float(sample[0])  < 0.94 ):
		while (len(mut[i][0])!=count):
			# check whetehr the mutation has fixed
			if i in fix_mut.keys():
				# check whether the mutation has fixed before the missing point
				if fix_mut[i]<=vec_gen[len(mut[i][0])]:
					mut[i][0].append(sample[len(mut[i][0])]);
				else:
					mut[i][0].append(0); 
			else:
				mut[i][0].append(0);
	else:
		mut.pop(i)

 # output the file in wfabc format
output=open(sys.argv[3],'w');

output.write(" ".join(str(e) for e in [len(mut.keys()),len(vec_gen)]));
output.write("\n");
output.write(' '.join(str(e) for e in vec_gen));
output.write("\n");
for i in mut.keys():
	output.write(' '.join(str(e) for e in sample));
	output.write("\n");
	output.write(' '.join(str(e) for e in mut[i][0]));
	output.write("\n");

output2=open(sys.argv[4],'w');

output2.write("Pos. Mut_type p_");
output2.write(" p_".join(str(e+1) for e in range(count)));
output2.write("\n");
for i in mut.keys():
	output2.write(mut[i][1]);
	output2.write(" ");
	output2.write(mut[i][2]);
	output2.write(" ");
	output2.write(" ".join(str(float(mut[i][0][e])/sample[e]) for e in range(count)));
	output2.write("\n");


